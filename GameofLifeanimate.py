import numpy as np
import matplotlib.pyplot as plt
from game_of_life import Game_of_Life
import argparse
from time import sleep
from matplotlib import animation


def main():

    parser = argparse.ArgumentParser(description='Simulation of Conway\'s Game of Life.')
    parser.add_argument('size', type=int, metavar='N', default=50, help='The size of the square lattice being simulated (NxN)')
    parser.add_argument('initial_condition', type=str, choices=['random', 'glider', 'blinker'], default='random', help='Initial condition of the grid: "random", "glider", or "blinker"')
    args = parser.parse_args()

    args = vars(args)

    initial_condition = args['initial_condition']
    size = (args['size'], args['size'])

    model = Game_of_Life(size=size, initial_condition=initial_condition, plotting=False)

    fig = plt.figure()
    im = plt.imshow(model.states, vmin=0, vmax=1)
    plt.colorbar()

    def update(i):
        model.step()
        im.set_array(model.states)
        return im

    ani = animation.FuncAnimation(fig, update, frames=10000, interval=10)

    plt.show()


if __name__ == "__main__":
    main()