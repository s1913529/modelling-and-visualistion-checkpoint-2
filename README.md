# Modelling and Visualistion Checkpoint 2

Epidemic simulation with the SIRS model.

gameoflife.py and sirs.py provide the classes used for the simulation.
They are both extensively documented with docstrings.

gameoflife provides Game_of_Life instance,
sirs provides SIRS and SIRSI instances.

# To animate

Game of life:

python GameofLifeanimate.py SIZE INITIAL_CONDITION


Replace the following arguments with the desired values:

    SIZE: The size of the square lattice being simulated (NxN).
    INITIAL_CONDITION: The initial condition of the grid. Choose from 'random', 'glider', or 'blinker'.


SIRSI:

python  SIRSanimate.py SIZE P1 P2 P3 IMMUNE

Replace the following arguments with the desired values:

    SIZE: The size of the square lattice being simulated (NxN).
    P1: Probability of infection.
    P2: Probability of recovery.
    P3: Probability of becoming susceptible again.
    IMMUNE: Fraction of immune agents.


All plotting was done on the Plotting.ipynb file. Corresponding data files are found in the root directory. 
