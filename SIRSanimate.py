import numpy as np
import matplotlib.pyplot as plt
from sirs import *
import argparse
from time import sleep
from matplotlib import animation



def main():

    parser = argparse.ArgumentParser(description='Simulation of SIRS epidemic model.')
    parser.add_argument('size', type=int, metavar='N', default = 50, help='The size of the square lattice being simulated (NxN)')
    parser.add_argument('p1', type=float, help='Probability of infection')
    parser.add_argument('p2', type=float, help='Probability of recovery')
    parser.add_argument('p3', type=float, help='Probability of becoming suceptible again')
    parser.add_argument('immune', type=float, help='Fraction of immune agents')
    args = parser.parse_args()

    args = vars(args)

    p1, p2, p3, immune_frac = args['p1'],args['p2'],args['p3'],args['immune']
    size = (args['size'],args['size'])

    model = SIRSI(p1,p2,p3,immune_frac, size = size, plotting=False)

    fig = plt.figure()
    im = plt.imshow(model.states, vmin=0,vmax=3)
    plt.colorbar()
    
    def update(i):
        model.sweep()
        im.set_array(model.states)
        return im

    ani = animation.FuncAnimation(fig, update,frames=10000, interval=10)

    plt.show()
    
if __name__=="__main__":
    main()