"""
SIRS Model
"""


import numpy as np
import matplotlib.pyplot as plt
class SIRS(object):
    
    def __init__(self,p1,p2,p3, size = (50,50), plotting = True):
    
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3
        self.size = size
        self.states = np.random.randint(0,high=3, size=size)

        self.plotting = plotting
    
        if plotting:
            plt.ion()
            self.fig, self.ax = plt.subplots(1,1)
            self.ax.set_aspect('equal')
    
    def update(self):
        
        random_index = (np.random.randint(0, self.size[0]),np.random.randint(0,self.size[1]))
        site = self.states[random_index]
        i, j = random_index
        size_y, size_x = self.size

        if site == 0:
            north = self.states[i-1, j]
            south = self.states[(i+1)%size_y, j]
            east = self.states[i, (j+1)%size_x]
            west = self.states[i, j-1]
            neighbours = np.array([
                north,
                south,
                east,
                west
            ])
            if len(neighbours[neighbours == 1]) >=1:
                random_num = np.random.rand()
                if random_num < self.p1:
                    self.states[random_index] = 1
        
        elif site == 1:
            random_num = np.random.rand()
            
            if random_num < self.p2:
                self.states[random_index] = 2
        
        else:
            random_num = np.random.rand()

            if random_num < self.p3:
                self.states[random_index] = 0
    
    def sweep(self):
        
        for i in range(self.size[0]*self.size[1]):
            self.update()