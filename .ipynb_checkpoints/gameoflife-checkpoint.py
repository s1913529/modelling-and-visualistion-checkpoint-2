import numpy as np
import matplotlib.pyplot as plt




class Game_of_Life(object):

    def __init__(self, size=(50,50), initial_condition='random', plotting= True):
        
        self.condition = initial_condition
        
        if initial_condition == 'random':
            states = np.random.randint(0, 2, size=size)

        elif initial_condition == 'glider':
            states = np.zeros(size)
            states[-1,1] = 1
            states[-2,2] = 1
            states[-3,0:3] = 1
        
        elif initial_condition == 'blinker':
            states = np.zeros(size)
            index_x = int(size[0]/2)
            index_y = int(size[1]/2)
            states[index_x-1:index_x+2, index_y] = 1
        
        else:
            raise ValueError

        self.states = states
        
        if plotting:
            plt.ion()
            self.fig, self.ax = plt.subplots(1,1)
            self.ax.set_aspect('equal')

        
    def step(self):
        new_states = np.zeros_like(self.states)
        size_x, size_y = self.states.shape

        for i in range(self.states.shape[0]):
            for j in range(self.states.shape[1]):
                north = self.states[i-1, j]
                south = self.states[(i+1)%size_y, j]
                east = self.states[i, (j+1)%size_x]
                west = self.states[i, j-1]
                northeast = self.states[i-1, (j+1)%size_x]
                northwest = self.states[i-1, j-1]
                southeast = self.states[(i+1)%size_y, (j+1)%size_x]
                southwest = self.states[(i+1)%size_y, j-1]

                neighbours = np.array([
                    north,
                    south,
                    east,
                    west,
                    northeast,
                    northwest,
                    southeast,
                    southwest
                ])
                
                n_neighbours = np.count_nonzero(neighbours)

                if n_neighbours < 2:
                    new_states[i,j] = 0
                
                elif n_neighbours == 2:
                    new_states[i,j] = self.states[i,j]
                
                elif n_neighbours == 3:
                    new_states[i,j] = 1
                
                else:
                    new_states[i,j] = 0
        
        self.states = new_states

        return

    def plot(self):
        self.ax.imshow(self.states)
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()
        return self.fig
    
    def com(self):
        coordinates = np.array(np.where(self.states == 1))
        self.comarray = coordinates.mean(axis = 1)
        return self.comarray
                