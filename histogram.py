from gameoflife import Game_of_Life
from multiprocessing import Pool
import numpy as np
from datetime import datetime
def runtoconverge(a):
    A = Game_of_Life(plotting=False)
    i = 0
    active_sites = 0
    identical_steps = 0
    while i in range(1000000):
        last_active_site = active_sites
        before = A.states
        A.step()
        after = A.states
        active_sites = np.count_nonzero(before - after)
        i += 1
        #print(i, active_sites)
        if active_sites == last_active_site:
            identical_steps += 1
        else:
            identical_steps = 0
        
        if identical_steps >=10:
            break
    return i




with Pool(14) as p:
    iterations = list(p.map(runtoconverge, [x for x in range(1000)]))

with open('hist_runs.txt', 'w') as f:
    for num in iterations:
        f.write(f'{num}\n')
    
