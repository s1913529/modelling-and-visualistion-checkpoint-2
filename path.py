from sirs import SIRS
from multiprocessing import Pool
import numpy as np
from tqdm import tqdm
p1_vals = np.arange(0,1,0.001)
p3_vals = np.arange(0,1,0.001)



def calc_point(p):
    A = SIRS(p, 0.5, 0.5, plotting=False)
    for i in range(100):
        A.sweep()
    values = []
    for i in range(10000):
        values.append(A.mean_infected())
        A.sweep()
    np.savetxt(f"{p}_var_raw.txt",values, delimiter=',')
    return np.mean(values), np.var(values)

def main():


    p_vals = np.linspace(0.2,0.5,100)


    with Pool(15) as p:
        measurements = list(tqdm(p.imap(calc_point, p_vals), total=100))

    np.savetxt('variance_1.txt', measurements,delimiter=',')


if __name__ == '__main__':
    main()