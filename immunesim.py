import numpy as np
from multiprocessing import Pool
from tqdm import tqdm
from sirs import *


def run(immune):
    model = SIRSI(0.5,0.5,0.5,immune, plotting=False)
    measure = []
    for j in range(10):
        model.sweep()
    for k in range(1000):
        measure.append(model.mean_infected())
        model.sweep()
    return np.mean(measure)


def main():
    args = np.linspace(0,1,100)
    with Pool(15) as p:
        vals = list(tqdm(p.imap(run, args), total=100))

    np.savetxt('immune.txt', vals, delimiter=',')
if __name__ == "__main__":
    main()