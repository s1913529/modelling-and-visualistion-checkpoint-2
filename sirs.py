"""
SIRS Model
"""


import numpy as np
import matplotlib.pyplot as plt

class SIRS(object):
    """
    A class representing a Susceptible-Infected-Recovered-Susceptible (SIRS) model simulation.

    Attributes:
        p1 (float): Transition probability from susceptible to infected.
        p2 (float): Transition probability from infected to recovered.
        p3 (float): Transition probability from recovered to susceptible.
        size (tuple): Dimensions of the simulation grid.
        states (np.array): 2D numpy array representing the grid of cells in the simulation.
        plotting (bool): A boolean indicating whether to enable plotting.
        fig (plt.Figure): A matplotlib Figure object for plotting, if plotting is enabled.
        ax (plt.Axes): A matplotlib Axes object for plotting, if plotting is enabled.
    """

    def __init__(self,p1,p2,p3, size = (50,50), plotting = True):
        """
        Initializes the SIRS object with specified grid size and transition probabilities.

        Args:
            p1 (float): Transition probability from susceptible to infected.
            p2 (float): Transition probability from infected to recovered.
            p3 (float): Transition probability from recovered to susceptible.
            size (tuple): A tuple of integers representing the dimensions of the grid (rows, columns).
            plotting (bool): A boolean indicating whether to enable plotting.
        """
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3
        self.size = size
        self.states = np.random.randint(0,high=3, size=size)

        self.plotting = plotting
    
        if plotting:
            #plt.ion()
            self.fig, self.ax = plt.subplots(1,1)
            self.ax.set_aspect('equal')
    
    def update(self):
        """
        Updates the state of a randomly chosen cell in the grid based on its neighbors and transition probabilities.
        """
        random_index = (np.random.randint(0, self.size[0]),np.random.randint(0,self.size[1]))
        site = self.states[random_index]
        i, j = random_index
        size_y, size_x = self.size

        if site == 0:
            north = self.states[i-1, j]
            south = self.states[(i+1)%size_y, j]
            east = self.states[i, (j+1)%size_x]
            west = self.states[i, j-1]
            neighbours = np.array([
                north,
                south,
                east,
                west
            ])
            if len(neighbours[neighbours == 1]) >=1:
                random_num = np.random.rand()
                if random_num < self.p1:
                    self.states[random_index] = 1
        
        elif site == 1:
            random_num = np.random.rand()
            
            if random_num < self.p2:
                self.states[random_index] = 2
        
        elif site == 2:
            random_num = np.random.rand()

            if random_num < self.p3:
                self.states[random_index] = 0

        elif site == 3:
            pass

    def sweep(self):
        """
        Performs one sweep of the grid, updating the state of each cell once.
        """
        for i in range(self.size[0]*self.size[1]):
            self.update()
    
    def mean_infected(self):
        """
        Computes the mean fraction of infected cells in the grid.

        Returns:
            float: The mean fraction of infected cells.
        """
        return np.count_nonzero(self.states==1)/(self.size[0]*self.size[1])
    
    def plot(self):
        """
        Plots the current state of the grid.
        """
        self.fig.clf()
        self.ax.imshow(self.states)


class SIRSI(SIRS):
    """
    A class representing a Susceptible-Infected-Recovered-Susceptible-Immune (SIRSI) model simulation,
    which is a variation of the SIRS model including immune cells.

    The suceptible cells are represented by the integer 0.
    The infected cells are represented by the integer 1.
    The recovered cells are represented by the integer 2.
    The immune cells are represented by the integer 3.

    This class inherits from the SIRS class.
    Attributes:
        immune_frac (float): The initial fraction of immune cells in the grid.
    """
    def __init__(self, p1,p2,p3,immune_frac, size=(50,50), plotting=True):
        #SIRS.__init__(self, p1, p2, p3, size = size, plotting=plotting)
        """
        Initializes the SIRSI object with specified grid size, transition probabilities, and immune fraction.

        Args:
            p1 (float): Transition probability from susceptible to infected.
            p2 (float): Transition probability from infected to recovered.
            p3 (float): Transition probability from recovered to susceptible.
            immune_frac (float): The initial fraction of immune cells in the grid.
            size (tuple): A tuple of integers representing the dimensions of the grid (rows, columns).
            plotting (bool): A boolean indicating whether to enable plotting.
        """
        super().__init__(p1,p2,p3,size=size,plotting=plotting)
        self.immune_frac = immune_frac

        for i in range(int(self.immune_frac*self.size[0]*self.size[1])):
            rand_index = (np.random.randint(0, self.size[0]),np.random.randint(0,self.size[1]))
            self.states[rand_index] = 3
